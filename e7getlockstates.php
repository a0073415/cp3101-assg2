<?php

/* ------------------------------------------------
 e7getlockedstates.php:

 Retrieves the current state locks in a particular world.

 Parameters:

 worldname - name of the world to be retrieved

 Returns:
 { status: "ok", locked_states : <locks array> } on success
 { status: "<error messages>" } on failure

 ------------------------------------------------ */

require_once "includes/constants.inc";

if ($_SERVER['REQUEST_METHOD'] === "POST") {
	if (isset($_POST[WORLDNAME])) {

		$worldname = $_POST[WORLDNAME];

		echo json_encode(array(LOCKED_STATES => getLockedStates($worldname), STATUS => OK_STRING));
	} else
		echo json_encode(array(STATUS => INVALID_REQUEST_STRING));

} else {
	echo json_encode(array(STATUS => INVALID_REQUEST_STRING));
}
