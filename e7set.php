<?php

/* ------------------------------------------------
 e7set.php:

 Posts a change to the server. 

 Parameters:

 delta - json encoded format to be committed  e.g {"o1": {x: 20, y: 15 }} Move o1 by 20 pixels right and 15 pixels down
 worldname - world to be changed

 Returns:
 { status: "ok", result: true if changed, false if otherwise, delta: <delta array posted> } on success
 { status: "<error messages>" } on failure

 ------------------------------------------------ */
 
require_once "includes/constants.inc";

if ($_SERVER['REQUEST_METHOD'] === "POST") {

	if (isset($_POST[$DELTA]) && isset($_POST[WORLDNAME]) && isset($_SESSION[USER])) {
		$deltaarray = json_decode($_POST[DELTA], true);

		$worldn = $_POST[WORLDNAME];

		$result = updateWorldDelta($worldn, $deltaarray, session_id());

		echo json_encode(array('delta' => $deltaarray, RESULT=> $result, STATUS => OK_STRING));
	} else
		echo json_encode(array(STATUS => INVALID_REQUEST_STRING));
} else
	echo json_encode(array(STATUS => INVALID_REQUEST_STRING));
