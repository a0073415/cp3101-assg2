<?php

require_once "includes/constants.inc";

if ($_SERVER['REQUEST_METHOD'] === "POST"){
	
	if (isset($_POST[$STATE]) && isset($_POST[WORLDNAME])){
		
		$state = json_decode($_POST[$STATE], true);	
		
		updateWorld($_POST[WORLDNAME], $state);
		echo json_encode($state);
	}else
		echo "Invalid request.";
	
}else{
	echo "Invalid request.";
}
	
