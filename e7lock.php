<?php

/* ------------------------------------------------
 e7lock.php:

 Attempts to acquire a lock on a piece in a world.

 Parameters:

 worldname - name of the world to be retrieved
 piece - piece to acquire lock on (e.g "o2")

 Returns:
 { status: "ok", result: true/false } on success
 { status: "<error messages>" } on failure

 ------------------------------------------------ */

require_once "includes/constants.inc";

//debug method
// var_dump(testAndSetLock('Pluto', 'x1', 'kernc'));
// releaseLock('Pluto', 'x1');

if ($_SERVER['REQUEST_METHOD'] === "POST") {

	$returnarray = array();

	$result = false;

	if (isset($_POST[PIECE]) && isset($_POST[WORLDNAME]) && isset($_SESSION[USER][$_POST[WORLDNAME]])) {
		if (isset($_POST[RELEASE])) {
			//release lock

			$result = releaseLock($_POST[WORLDNAME], $_POST[PIECE], session_id());
		} else {
			//set lock

			$result = testAndSetLock($_POST[WORLDNAME], $_POST[PIECE], session_id());
		}

		echo json_encode(array(STATUS => OK_STRING, RESULT => $result));
	} else {
		echo json_encode(array(STATUS => INVALID_REQUEST_STRING));
	}

} else
	echo json_encode(array(STATUS => INVALID_REQUEST_STRING));
