<?php

require_once "includes/constants.inc";

if ($_SERVER['REQUEST_METHOD'] === "POST") {
	if (isset($_POST[WORLDNAME])) {

		$worldname = $_POST[WORLDNAME];

		$db = getDB(DB_NAME);

		$worlds = $db -> worlds;
		$doc = $worlds -> findOne(array(WORLDNAME => $worldname));

		$returnarray = array();

		if (!is_null($doc)) {

			$returnarray = $doc[STATE];

		}
		echo json_encode($returnarray);
	} else
		echo "Invalid request.";

} else {
	echo "Invalid request.";
}
