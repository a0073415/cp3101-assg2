<?php
/* ------------------------------------------------
 e7get.php:

 Retrieves the current state of a particular world

 Parameters:

 worldname - name of the world to be retrieved

 Returns:
 { status: "ok", state: <worldstate>, participants : <participants array> } on success
 { status: "<error messages>" } on failure

 ------------------------------------------------ */
require_once "includes/constants.inc";

$status = "";

if ($_SERVER['REQUEST_METHOD'] === "POST") {
	if (isset($_POST[WORLDNAME])) {

		$worldname = $_POST[WORLDNAME];

		$db = getDB(DB_NAME);

		$worlds = $db -> worlds;
		$doc = $worlds -> findOne(array(WORLDNAME => $worldname));

		if (!is_null($doc)) {

			$tparticipantsarray = $doc[PARTICIPANTS];
			$retworldstate = $doc[STATE];

			$participantsarray = array();

			// filter out self
			foreach ($tparticipantsarray as $key => $part) {
				if ($key != session_id()) {
					$participantsarray[$key] = $part;
				}
			}

		} else {

			// Return newly initialized world state
			$retworldstate = createAndInitializeWorld($worldname);
			$participantsarray = array();
		}
		$status = OK_STRING;
		$returnarray = array(STATE => $retworldstate, PARTICIPANTS => $participantsarray, STATUS => $status);
		echo json_encode($returnarray);
	} else
		echo json_encode(array(STATUS => INVALID_REQUEST_STRING));

} else {
	echo json_encode(array(STATUS => INVALID_REQUEST_STRING));
}
