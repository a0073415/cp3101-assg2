<?php

/* ------------------------------------------------
 e7screenset.php:

 Sets or updates the player's screen size to the server

 Parameters:

 worldname - name of the world to be retrieved
 screen - size of players screen in json encoded format {screen_x: int, screen_y: int }

 Returns:
 { status: "ok", data: {screen_x: int, screen_y: int } } on success
 { status: "<error messages>" } on failure

 ------------------------------------------------ */

require_once "includes/constants.inc";

if ($_SERVER['REQUEST_METHOD'] === "POST") {

	//sets screen
	if (isset($_POST[SCREEN]) && isset($_POST[WORLDNAME])) {

		$screenupdate = json_decode($_POST[SCREEN], true);

		setScreenSize($_POST[WORLDNAME], session_id(), $screenupdate[SCREEN_X], $screenupdate[SCREEN_Y]);

		echo json_encode(array(STATUS => OK_STRING, DATA => $screenupdate));
	} else {
		echo json_encode(array(STATUS => INVALID_REQUEST_STRING));
	}
} else
	echo json_encode(array(STATUS => INVALID_REQUEST_STRING));
