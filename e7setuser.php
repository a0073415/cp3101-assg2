<?php
/* ------------------------------------------------
 e7setuser.php:

 Registers a user in a world.

 Parameters:

 worldname - world to be registered in
 user - username to be registered. Username is simply a nickname.

 Returns:
 { status: "ok", user: <user posted>} on success
 { status: "<error messages>" } on failure

 ------------------------------------------------ */
 
require_once "includes/constants.inc";

if ($_SERVER['REQUEST_METHOD'] === "POST") {

	if (isset($_POST[WORLDNAME])) {
		$worldname = $_POST[WORLDNAME];

		if (!isset($_SESSION[USER])) {
			$_SESSION[USER] = array();
		}

		if (isset($_POST[USER])) {

			if (isset($_SESSION[USER][$worldname]) && ($_SESSION[USER][$worldname] == $_POST[USER])) {
			} else {
				//set user
				$_SESSION[USER][$worldname] = $_POST[USER];
				setUser($worldname, session_id(), $_POST[USER]);
			}
		}
		if (isset($_SESSION[USER][$worldname])) {
			$user = $_SESSION[USER][$worldname];
		} else {
			$user = '';
		}

		echo json_encode(array(USER => $user, STATUS => OK_STRING));

	} else
		echo json_encode(array(STATUS => INVALID_REQUEST_STRING));

} else
	echo json_encode(array(STATUS => INVALID_REQUEST_STRING));
