<?php

/* ------------------------------------------------
 e8deleteworld.php:

 Deletes the world from the database. World must be empty or false will be returned.

 Parameters:

 worldname - world to be deleted

 Returns:
 { status: "ok", result: true if deleted, false if otherwise } on success
 { status: "<error messages>" } on failure

 ------------------------------------------------ */

require_once "includes/constants.inc";

if ($_SERVER['REQUEST_METHOD'] === "POST") {

	if (isset($_POST[WORLDNAME])) {
		$worldname = $_POST[WORLDNAME];

		removeScreenSize($_POST[WORLDNAME], session_id());
		$result = deleteWorld($worldname);
		echo json_encode(array(STATUS => OK_STRING, RESULT => $result));
	} else
		echo json_encode(array(STATUS => INVALID_REQUEST_STRING));

} else
	echo json_encode(array(STATUS => INVALID_REQUEST_STRING));
