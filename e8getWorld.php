<?php

/* ------------------------------------------------
 e8getWorld.php:

 Retrieves the worlds from the database.

 Returns:
 { status: "ok", worlds: <world array> } on success
 { status: "<error messages>" } on failure

 ------------------------------------------------ */

require_once "includes/constants.inc";

if ($_SERVER['REQUEST_METHOD'] === "POST") {
	echo json_encode(array(STATUS => OK_STRING, 'worlds' => getWorlds()));
} else {
	echo json_encode(array(STATUS => INVALID_REQUEST_STRING));
}
