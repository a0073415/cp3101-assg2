<?php

/* ------------------------------------------------
 e7shutdown.php:

 Posts a change to the server. 

 Parameters:

 worldname - world to unregister user from

 ------------------------------------------------ */

require_once "includes/constants.inc";

if ($_SERVER['REQUEST_METHOD'] === "POST") {

	if (isset($_POST[WORLDNAME])) {
		$worldname = $_POST[WORLDNAME];
		
		unset($_SESSION[USER][$worldname]);
		removeScreenSize($_POST[WORLDNAME], session_id());
	}

}
