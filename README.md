# Deployment Instructions

## Deployment Requirements

This project requires mod_rewrite and mod_ssl installed in an Apache Server.
It is recommended that the server has git installed.
MongoDB is used in this project and it is assumed that it has been set up 
appropriately

## Deployment Guide


1. Head to web server directory

2. git clone https://a0073415@bitbucket.org/a0073415/cp3101-assg2.git

		Check if last commit is : 
		README Committed: 28 March 2014
	
	OR
	
	Unzip the submitted zip (more tedious)

3. Open up config/config.inc and edit the following lines:

	    define('DB_NAME', 'cp3101b');
	    
	    define('DB_USER', 'group4');
	    
	    define('DB_PASS', 'password4');
	    
	    define('DB_HOST', 'localhost');
    
	to the respective values.

4. Fire up https://<webserver>/e6.html , https://<webserver>/e7.html or https://<webserver>/e8.html

5. If there are javascript errors, it is likely that the MongoDB backend is configured wrongly. Please empty the 'worlds' collection if neccessary.

## Deployed URL (for reference)

You can find a deployed version for reference [here](https://cp3101b-1.comp.nus.edu.sg/~a0073415/cp3101-assg2/e8.html)

