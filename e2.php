<?php

require_once "includes/constants.inc";

if ($_SERVER['REQUEST_METHOD'] === "POST"){
	
	if (isset($_POST[$STATE])){
		$state = json_decode($_POST[$STATE], true);
		
		echo json_encode($state);
	}else
		echo "Invalid request.";
	
}else{
	echo "Invalid request.";
}
	
