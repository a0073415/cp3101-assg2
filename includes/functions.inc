<?php
/* Database manipulation Helper methods */

function getDB($dbname) {
	$m = new MongoClient("mongodb://" . DB_HOST, array("username" => DB_USER, "password" => DB_PASS, "db" => $dbname));
	return $m -> $dbname;
}

// Create or Update the world state in the Database
function updateWorld($worldname, $worldstate) {
	// Create or update a world object
	$db = getDB(DB_NAME);

	$worlds = $db -> worlds;
	// Assume empty participants
	$worldarray = array(WORLDNAME => $worldname, PARTICIPANTS => array(), STATE => $worldstate);

	$db -> worlds -> findAndModify(array('worldname' => $worldarray[WORLDNAME]), $worldarray, null, array("upsert" => true, "new" => true));
}

/* Methods used in E7 and E8
 * From E7, deltas are used to update state changes
 * Locks can be requested for particular objects in particular worlds*/
function createAndInitializeWorld($worldname) {

	// creates new world. Upsert is still used to ensure concurrent create requests will not cause any errors
	// they will just be created twice.

	$db = getDB(DB_NAME);
	$d = new \stdClass();

	$init_state = array('x' => 35, 'y' => 35);

	$state = array('x1' => $init_state, 'x2' => $init_state, 'x3' => $init_state, 'x4' => $init_state, 'x5' => $init_state, 'o1' => $init_state, 'o2' => $init_state, 'o3' => $init_state, 'o4' => $init_state, 'o5' => $init_state, 'tttboard' => $init_state);

	$locked = array('x1' => array(LOCKED => false, USER => ''), 'x2' => array(LOCKED => false, USER => ''), 'x3' => array(LOCKED => false, USER => ''), 'x4' => array(LOCKED => false, USER => ''), 'x5' => array(LOCKED => false, USER => ''), 'o1' => array(LOCKED => false, USER => ''), 'o2' => array(LOCKED => false, USER => ''), 'o3' => array(LOCKED => false, USER => ''), 'o4' => array(LOCKED => false, USER => ''), 'o5' => array(LOCKED => false, USER => ''), 'tttboard' => array(LOCKED => false, USER => ''));
	$worldarray = array(WORLDNAME => $worldname, PARTICIPANTS => $d, STATE => $state, LOCKED => $locked);
	$worldobj = $db -> worlds -> findAndModify(array(WORLDNAME => $worldname), $worldarray, null, array("upsert" => true, "new" => true));

	return $state;
}

// incrementally updates an element in a world.
function updateWorldDelta($worldname, $statedelta, $sess) {
	$db = getDB(DB_NAME);
	// Create or update a world object

	$worldobj = $db -> worlds -> findOne(array(WORLDNAME => $worldname));
	$result = true;
	if (!is_null($worldobj)) {

		foreach ($statedelta as $key => $value) {
			if ($worldobj[LOCKED][$key][USER] == $sess) {
				//only allow modifications if lock is acquired by user
				$db -> worlds -> findAndModify(array(WORLDNAME => $worldname), array('$inc' => array(STATE . "." . $key . ".x" => intval($value['d_x']), "state." . $key . ".y" => intval($value['d_y']), )), null, array("upsert" => true, "new" => true));
			} else {
				$result = false;
			}
		}
		return $result;
	}

	return false;

}

// Attempts to acquire the lock. Returns true if successful, false if lock is already held
function testAndSetLock($worldname, $elementname, $sess) {

	$db = getDB(DB_NAME);
	$worlds = $db -> worlds;
	$doc = $worlds -> findOne(array(WORLDNAME => $worldname));

	if (!is_null($doc)) {

		if ($doc[LOCKED][$elementname][LOCKED]) {
			//if already locked

		} else {
			// attempt to acquire lock
			$worlds -> findAndModify(array(WORLDNAME => $worldname), array('$set' => array(LOCKED . '.' . $elementname . '.' . LOCKED => true, LOCKED . '.' . $elementname . '.' . USER => $sess, PARTICIPANTS . '.' . $sess . '.' . PIECE => $elementname)), null, array("upsert" => true, "new" => true));
			// $worlds -> findAndModify(array(WORLDNAME => $worldname), array('$set' => array()), null, array("upsert" => true, "new" => true));
			return true;
		}

	}

	return false;
}

function releaseLock($worldname, $elementname, $sess) {

	$db = getDB(DB_NAME);
	$worlds = $db -> worlds;
	$doc = $worlds -> findOne(array(WORLDNAME => $worldname));
	if (!is_null($doc)) {

		if ($doc[LOCKED][$elementname][USER] == $sess) {
			$db -> worlds -> findAndModify(array(WORLDNAME => $worldname), array('$set' => array(LOCKED . '.' . $elementname . '.' . LOCKED => false, LOCKED . '.' . $elementname . '.' . USER => "", PARTICIPANTS . '.' . $sess . '.' . PIECE => '')), null, array("upsert" => true, "new" => true));
			// $worlds -> findAndModify(array(WORLDNAME => $worldname), array('$set' => array()), null, array("upsert" => true, "new" => true));
		}
	}

	return true;
}

function getLockedStates($worldname) {

	$db = getDB(DB_NAME);

	$worlds = $db -> worlds;
	$doc = $worlds -> findOne(array(WORLDNAME => $worldname));

	$returnarray = array();

	if (!is_null($doc)) {
		$returnarray = $doc[LOCKED];
	}
	return $returnarray;
}

function setUser($worldname, $sess_id, $user) {

	$db = getDB(DB_NAME);
	$worlds = $db -> worlds;

	$worlds -> findAndModify(array(WORLDNAME => $worldname), array('$set' => array(PARTICIPANTS . '.' . $sess_id . '.' . USER => $user)), null, array("upsert" => true, "new" => true));
	return true;
}

// Sets the username and screen size of a user
function setScreenSize($worldname, $sess_id, $screen_x, $screen_y) {

	$db = getDB(DB_NAME);
	$worlds = $db -> worlds;

	$worlds -> findAndModify(array(WORLDNAME => $worldname), array('$set' => array(PARTICIPANTS . '.' . $sess_id . '.' . SCREEN_X => $screen_x, PARTICIPANTS . '.' . $sess_id . '.' . SCREEN_Y => $screen_y)), null, array("upsert" => true, "new" => true));
	return true;
}

function removeScreenSize($worldname, $sess_id) {
	$db = getDB(DB_NAME);
	$worlds = $db -> worlds;

	$worlds -> update(array(WORLDNAME => $worldname), array('$unset' => array(PARTICIPANTS . '.' . $sess_id => 1)));
	return true;
}

// Returns the screen sizes of the users in the world
// Can be used to retrieve user nicknames as well
// Check if result is null before using
function getScreenSize($worldname) {

	$db = getDB(DB_NAME);

	$worldobj = $db -> worlds -> findOne(array(WORLDNAME => $worldname));
	// PARTICIPANTS => array('$elemMatch' => array(USER => $user)))
	if (!is_null($world)) {
		$participants_array = $worldobj[PARTICIPANTS];
		return $participants_array;
	}

	return null;
}

// Deletes a world
function deleteWorld($worldname) {
	//check if there are any participants in the world
	$db = getDB(DB_NAME);
	$w = $db -> worlds -> findOne(array(WORLDNAME => $worldname));
	if (!is_null($w)) {
		//var_dump($w[PARTICIPANTS]);
		$newarr = array_filter($w[PARTICIPANTS]);
		if (empty($newarr)) {
			// proceed with delete
			$db -> worlds -> remove(array(WORLDNAME => $worldname));
			return true;
		}
	}
	return false;

}

/* USED IN e8 */
function getWorlds() {
	$db = getDB(DB_NAME);
	$worlds = $db -> worlds -> find();
	$returnworlds = array();
	if ($worlds -> count() != 0) {
		while ($doc = $worlds -> getNext()) {
			$returnworlds[] = $doc;
		}
	}
	return $returnworlds;
}
