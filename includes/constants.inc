<?php

// old constants used for previous exercises
$STATE = 'state';
$DELTA = 'delta';

define("STATUS", 'status');

define('WORLDNAME', 'worldname');
define('STATE', 'state');

define('DELTA', 'delta');
define('LOCKED_STATES', 'locked_states');
define('DATA', 'data');
define('RESULT', 'result');

define('USER', 'user');
define('PARTICIPANTS', 'participants');
define('LOCKED', 'locked');
define('PIECE', 'piece');
define('RELEASE', 'release');

define('SCREEN', 'screen');
define('SCREEN_X', 'screen_x');
define('SCREEN_Y', 'screen_y');

define("INVALID_REQUEST_STRING", "Invalid request!");
define("OK_STRING", "ok");

require_once dirname(__FILE__) . '/../config/config.inc';
require_once 'functions.inc';

if (PRODUCTION) {
	session_save_path('tmp');
}
session_start();
