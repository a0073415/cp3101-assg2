Solutions to E6

1) Multiple players moving the same block lead to contention

Players attempting to move a piece will have to acquire a lock that is stored on the object itself in the database.
Players that are not able to acquire the lock will not be able to commit state changes to the database. Any requests
to commit will be ignored.

2) If polling rate is too often, previous transaction may not have completed.
Race conditions may occur in the backend as well.

This can be arguably an architectural problem with using AJAX to poll for state changes. That being said,
queries related to reading and modifying states should be made as short as possible and the backend should try to do
as little work as possible. 

update and get functions are designed to be short and all database updates should be made in an atomic manner. 

3) The world state should be preserved and not roll back to a previous invalidated state.
This means when a player with an older state makes a change, the entire world state shouldn't roll back to 
that of the player which committed the last change.

Player changes are done in terms of deltas (incremental changes) rather than posting the entire state. This also
reduces workload on both the server and client. Players only post changes associated with the piece they have acquired a lock
upon. Changes are only posted to the server when they released the drag (dragstop event).

As MongoDB was used as the database of choice, we can make use of the $set and $inc operators for an atomic delta update.

4) Gradual changes should be observable by other users. Pieces shouldn't suddenly fly all over the place.

When a change of state is detected in the client side, pieces will animate to their location gradually instead of appearing
there immediately.

Players are also able to see pieces that are locked by other people (i.e other players are dragging) such that they can 
anticipate the piece will be moved.

5) Players should be aware that other users may have a different screen size from them. 

The problem with screen size is a touchy one. On one hand, we should discourage players to move pieces out of the screen 
of their fellow players. On the other hand, we cannot restrict them from doing so as this will cause players who (deliberately
or otherwise) set their screen to a very small size to disrupt the game. Thus, we decided to show the screen sizes of all the
(other) players in the world by drawing rectangle overlays in the world itself. A player can still drag a piece out of the 
rectangle overlay at their own will.

6) Users should not be able to act as other users. They shouldn't be able to spoof as another user and play as them.

Initially, users are not properly identified in e6. In e7, players are identified by their session ids. This means that
even if they try to spoof as another user by changing their nickname, it will not matter as their session id will not change.
Also, if they open multiple tabs, they are still playing as the same user essentially.

They can attempt to sniff out the session id though, but this problem will not be addressed in the assignment. We still
enforce the use of HTTPS to minimize this problem.

7) The issue on when should a world be initialized. Worlds are assumed to be initialized everytime a player joins. This 
means the world will reset everytime a new player joins, which is a problem if two people are playing midway and a 3rd
player joins the game.
The alternative is to initialize the world once and never reset it again. This means that players other players may join
an empty world to see the pieces all over the place.

The world is initialized on create. We will preseve the state of the world indefinitely until it is deleted. (Deletion of worlds
is only supported in E8)